<?php

namespace PhpIntegrator\Indexing\Structures;

/**
 * Enumeration of structure type name values.
 */
class StructureTypeNameValue
{
    /**
     * @var string
     */
    public const CLASS_ = 'class';

    /**
     * @var string
     */
    public const INTERFACE_ = 'interface';

    /**
     * @var string
     */
    public const TRAIT_ = 'trait';
}
