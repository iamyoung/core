<?php

namespace PhpIntegrator\Indexing\Structures;

/**
 * Enumeration of access modifier name values.
 */
class AccessModifierNameValue
{
    /**
     * @var string
     */
    public const PUBLIC_ = 'public';

    /**
     * @var string
     */
    public const PROTECTED_ = 'protected';

    /**
     * @var string
     */
    public const PRIVATE_ = 'private';
}
