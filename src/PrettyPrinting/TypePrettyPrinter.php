<?php

namespace PhpIntegrator\PrettyPrinting;

/**
 * Pretty prints types.
 */
class TypePrettyPrinter
{
    /**
     * @param string $type
     *
     * @return string
     */
    public function print(string $type): string
    {
        return $type;
    }
}
